<?php

namespace Drupal\countdown_timer\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "countdown_timer_block",
 *   admin_label = @Translation("Countdown Timer"),
 *   category = @Translation("Countdown Timer")
 * )
 */
class CountdownTimerBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['#theme'] = [
      'countdown_timer_block',
    ];

    $build['#config'] = [
      $this->configuration,
    ];

    $build['#attached'] = [
      'library' => 'countdown_timer/countdown_timer',
    ];

    dump($this->configuration);

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess( AccountInterface $account ) {
    return AccessResult::allowedIfHasPermission( $account, 'access content' );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm( $form, FormStateInterface $form_state ) {
    $config = $this->getConfiguration();

    $form['end_date_time'] = [
      '#type'          => 'datetime',
      '#title'         => $this->t( 'End date and time' ),
      '#default_value' => isset( $config['end_date_setting'] ) ? new DrupalDateTime( $config['end_date_setting'] ) : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit( $form, FormStateInterface $form_state ) {
    $this->configuration['end_date_setting'] = $form_state->getValue( 'end_date_time' )
                                                          ->__toString();
  }

}
