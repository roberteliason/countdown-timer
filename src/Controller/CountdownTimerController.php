<?php

namespace Drupal\countdown_timer\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Countdown Timer routes.
 */
class CountdownTimerController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function example() {
    return [
      '#theme' => 'example_page',
      '#attached' => [
        'library' => [
          'countdown_timer/countdown_timer',
        ],
        'drupalSettings' => [
          'countdown_timer' => [
            'settings' => [],
          ],
        ],
      ],
    ];
  }

}
