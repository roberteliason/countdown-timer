import Vue from 'vue'
// import App from './App.vue'
import CountDown from './CountDown.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(CountDown),
}).$mount('#app')
