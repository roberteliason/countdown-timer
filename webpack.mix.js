// There's some really cool stuff in the docs, read more about it here
// Documentation: https://laravel.com/docs/5.7/mix

let mix = require('laravel-mix');

mix.webpackConfig(webpack => {
  return {
    plugins: [
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development')
        }
      }),
    ]
  };
}).js('./assets/main.js', './assets/dist/js')
  .options({
    processCssUrls: false,
  })
  .sourceMaps();
